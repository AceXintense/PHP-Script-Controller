<?php
/**
 * This class allows the user to run a script once.
 */
include 'sys/core/Utils.php';
include 'sys/core/ScriptTemplate.php';
class One {

    public $helpArguments = [
        'help',
        'info'
    ];

    /**
     * Runs a Script that is created in the root directory.
     * This creates an instance of the script and also makes
     * sure that there is a script by the $scriptName
     *
     * @param $scriptName string
     * @param $arguments array
     */
    public function runScript($scriptName, $arguments) {
        system('clear');
        \Utilities\Utils::setup();
        define('DEBUG', \Utilities\Utils::getDebugStatus());
        \Utilities\Utils::log('Running Version : ' . \Utilities\Utils::version());
        \Utilities\Utils::log('Debug Mode : ' . DEBUG);

        $scriptName = $this->removePHPExtention($scriptName);
        if (file_exists($scriptName . '.php')) {
            include_once($scriptName . '.php');
            $script = new $scriptName();
            if (in_array($arguments[0], $this->helpArguments)) {
                $script->info($arguments);
            } else {
                $script->main($arguments);
            }
        } else {
            \Utilities\Utils::log('There\'s no such file with the name of ' . $scriptName . '.php');
        }

    }

    /**
     * Remove the .php from the end of the $scriptName
     * and return the result.
     *
     * @param $scriptName
     * @return string
     */
    private function removePHPExtention($scriptName) {
        $regex = '/(\w*)(\.php$)/';
        return preg_replace($regex, '$1', $scriptName);
    }

}

$instance = new One();
$scriptName = $argv[1];
for ($i = 0; $i < 2; $i++) {
    array_shift($argv);
}
$instance->runScript($scriptName, $argv);