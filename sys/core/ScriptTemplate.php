<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 12/3/16
 * Time: 8:29 PM
 */

namespace Template;


interface ScriptTemplate {

    function main($arguments);

    function info();

}